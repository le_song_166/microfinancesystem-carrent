const Car = require('../models/Cars')

const carService = {
	getCars: async () => {
		const cars = await Car.find()
		return cars;
	},
	getCarById: async (req) => {
		const cars = await Car.findById(req.params.carId);
		return cars;
	},
	createCar: async (req) => {
		const car = new Car({
			model: req.body.model,
			licensePlate: req.body.licensePlate,
			pricePerDay: req.body.pricePerDay,
		})
		const savedCar = await car.save();
		return savedCar;
	},
	updateCar: async (req) => {
		const updatedCar = await Car.updateOne({ _id: req.params.carId },
			{
				$set: {
					model: req.body.model,
					licensePlate: req.body.licensePlate,
					pricePerDay: req.body.pricePerDay
				}
			});
		return updatedCar;
	},
	deleteCar: async (req) => {
		const deletedCar = await Car.deleteOne({ _id: req.params.carId });
		return deletedCar;
	},
}
  
module.exports = carService;