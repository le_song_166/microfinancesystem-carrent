const DamagePayment = require('../models/DamagePayment')

const damagePaymentService = {
	getDamagePayments: async () => {
		const damagePayments = await DamagePayment.find()
		return damagePayments;
	},
	getDamagePaymentById: async (req) => {
		const damagePayment = await DamagePayment.findById(req.params.paymentId);
		return damagePayment;
	},
	saveDamagePayment: async (req) => {
		const damagePayment = new DamagePayment({
			rentId: req.body.rentId,
			amount: req.body.amount,
			transactionHash: req.body.transactionHash,
		})

		const savedDamagePayment = await damagePayment.save();
		return savedDamagePayment;
	},
	updateDamagePayment: async (req) => {
		const updatedPayment = await DamagePayment.updateOne({ _id: req.params.paymentId },
			{
				$set: {
					rentId: req.body.rentId,
					amount: req.body.amount,
					transactionHash: req.body.transactionHash,
				}
			});
		return updatedPayment;
	},
	deleteDamagePayment: async (req) => {
		const payment = await DamagePayment.deleteOne({ _id: req.params.paymentId });
		return payment;
	}

}
  
module.exports = damagePaymentService;