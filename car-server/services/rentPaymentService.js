const RentPayment = require('../models/RentPayment')

const rentPaymentService = {
	getRentPayments: async () => {
		const rentPayments = await RentPayment.find()
		return rentPayments;
	},
	getRentPaymentById: async (req) => {
		const rentPayment = await RentPayment.findById(req.params.paymentId);
		return rentPayment;
	},
	saveRentPayment: async (req) => {
		const rentPayment = new RentPayment({
			rentId: req.body.rentId,
			amount: req.body.amount,
			transactionHash: req.body.transactionHash,
		})

		const savedRentPayment = await rentPayment.save();
		return savedRentPayment;
	},
	updateRentPayment: async (req) => {
		const updatedPayment = await RentPayment.updateOne({ _id: req.params.paymentId },
			{
				$set: {
					rentId: req.body.rentId,
					amount: req.body.amount,
					transactionHash: req.body.transactionHash,
				}
			});
		return updatedPayment;
	},
	deleteRentPayment: async (req) => {
		const payment = await RentPayment.deleteOne({ _id: req.params.paymentId });
		return payment;
	}

}
  
module.exports = rentPaymentService;