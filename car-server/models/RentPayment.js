const mongoose = require('mongoose')

const RentPaymentSchema = mongoose.Schema({
    rentId: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    transactionHash: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('RentPayment', RentPaymentSchema)