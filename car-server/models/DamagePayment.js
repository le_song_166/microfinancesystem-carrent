const mongoose = require('mongoose')

const DamagePaymentSchema = mongoose.Schema({
    rentId: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    transactionHash: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('DamagePayment', DamagePaymentSchema)