const mongoose = require('mongoose')

const CarSchema = mongoose.Schema({
    model: {
        type: String,
        required: true
    },
    licensePlate: {
        type: String,
        required: true
    },
    pricePerDay: {
        type: Number,
        required: true
    }
})

module.exports = mongoose.model('Cars', CarSchema)