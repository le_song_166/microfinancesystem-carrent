const Express = require('express')
const DamagePayment = require('../models/DamagePayment');

const router = Express.Router()
const damagePaymentService = require('../services/damagePaymentService');

//GET ALL Damage Payments
/**
 * @swagger
 * components:
 *  schemas:
 *      DamagePayment:
 *          type: object
 *          required:
 *              -borrower
 *              -rentId
 *              -amount
 *              -transactionHash
 *          properties:
 *              _id:
 *                  type: string
 *                  description: The auto generated id from Mongo DB
 *              borrower:
 *                  type: string
 *                  description: Borrower address
 *              rentId:
 *                  type: number
 *                  description: Rent id
 *              amount:
 *                  type: number
 *                  description: Paid amount
 *              transactionHash:
 *                  type: sring
 *                  description: Blockchain transaction hash
 *          example:
 *              _id: 60d6ffbcc743bb4d6c69da68
 *              borrower: '0x940028a249EB48446dA6E68DD9A1927Cd4822A9f'
 *              rentId: 1
 *              amount: 300
 *              transactionHash: '0x0025a9562a86021ec187a33d6c3ad65a5ee3538b52e383769fec675fd500387d'
 *              
 */


/**
 * @swagger
 * tags:
 *  name: Damage Payments
 *  description: The Damage Payment API for the Microfinance
 */

/**
 * @swagger
 * /damage-payments:
 *  get:
 *      summary: Returns the list of all damage payment transactions
 *      tags: [Damage Payments]
 *      responses:
 *          200:
 *              description: The list of the rent plans
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items: 
 *                              $ref: '#/components/schemas/DamagePayment'
 */
router.get('/', async (req, res) => {
    try {
		const damagePayments = await damagePaymentService.getDamagePayments();
		res.json(damagePayments);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

//GET SPECIFIC PAYMENT DETAILS

/**
 * @swagger
 * /damage-payments/{paymentId}:
 *  get:
 *      summary: Get damage payment by id
 *      tags: [Damage Payments]
 *      parameters:
 *          - in: path
 *            name: paymentId
 *            schema:
 *              type: string
 *            required: true
 *            desciption: The damage payment id
 *      responses:
 *          200:
 *              description: The damage payment by id
 *              contents:
 *                  application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/DamagePayment'
 *          404:
 *              description: The damage payment was not found
 *                      
 *                  
 *          
 */
 router.get('/:paymentId', async (req, res) => {
    console.log(req.params.paymentId);
    try{
		const damagePayments = await damagePaymentService.getDamagePaymentById(req);
		res.json(damagePayments);
    }
    catch(err){
        res.json({
            message: err
        })
    }
    
})

// SUBMIT A DAMAGE PAYMENT

/**
 * @swagger
 * /damage-payments:
 *  post:
 *      summary: Add new damage payment entry
 *      tags: [Damage Payments]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/DamagePayment'
 *      responses:
 *          200:
 *              description: The damage payment was successfully created
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/DamagePayment'
 *          500:
 *              description: Some server error
 */
 router.post('/', async (req, res) => {
    try {
		const damagePayment = await damagePaymentService.saveDamagePayment(req);
		res.json(damagePayment);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

//UPDATE DAMAGE PAYMENT

/**
 * @swagger
 * /damage-payments/{paymentId}:
 *  patch:
 *      summary: Update the damage payment by Id
 *      tags: [Damage Payments]
 *      parameters:
 *          - in: path
 *            name: paymentId
 *            schema:
 *              type: string
 *            required: true
 *            description: The damage payment id
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/DamagePayment'
 *      responses:
 *          200:
 *              description: The damage payment was successfully created
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/DamagePayment'
 *          404:
 *              description: The damage payment is not found
 *          500:
 *              description: Some server error
 */
 router.patch('/:paymentId', async (req, res) => {
    console.log(req.params.paymentId);
    try {
        const updatedPayment = await damagePaymentService.updateDamagePayment(req);
        res.json(updatedPayment);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

//DELETE DAMAGE PAYMENT

/**
 * @swagger
 * /damage-payments/{paymentId}:
 *  delete:
 *      summary: Remove the damage payment by Id
 *      tags: [Damage Payments]
 *      parameters:
 *          - in: path
 *            name: paymentId
 *            schema:
 *              type: string
 *            required: true
 *            description: The damage payment id
 *      responses:
 *          200:
 *              description: The damage payment was successfully deleted
 *          404:
 *              description: The damage payment is not found
 *          500:
 *              description: Some server error
 */
 router.delete('/:paymentId', async (req, res) => {
    console.log(req.params.paymentId);
    try {
        const payment = await damagePaymentService.deleteDamagePayment(req);
        if(payment.deletedCount==0){
            res.status(404).send('Damage Payment not found');
        }
        res.json(payment);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

module.exports = router;
