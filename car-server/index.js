const Express = require('express')
const mongoose = require('mongoose')
const cors = require('cors');

const swaggerUi = require('swagger-ui-express')
const swaggerJsDoc = require('swagger-jsdoc')

//SWAGGER
const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Car API",
            version: "1.0.0",
            description: "Car API for Microfinance"
        },
        servers: [
            {
                url: "http://localhost:9093"
            }
        ],
    },
    apis: ["./routes/*.js"]
}

const specs = swaggerJsDoc(options)

const app = Express()

//Import Routed
const carsRoute = require('./routes/cars');
const rentpaymentsRoute = require('./routes/rentpayments');
const damagepaymentsRoute = require('./routes/damagepayments');

//MIDDLEWARE
app.use(cors())

app.use(Express.urlencoded({ extended: true }));
app.use(Express.json())

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(specs))

app.use('/cars', carsRoute);
app.use('/rent-payments', rentpaymentsRoute);
app.use('/damage-payments', damagepaymentsRoute);

//ROUTES
app.get('/', (req, res) => {
    res.send('Welcome to Car Server')
})

//Connect to DB
const url = 'mongodb://127.0.0.1:27017/cars-db'
mongoose.connect(
    url,
    { useNewUrlParser: true },
    () => {
        console.log('connected to Car DB')
    })


//LISTENING
app.listen(9093)
