import React from 'react';
import { Row, Col } from 'antd';
import RentForm from '../../components/rent/RentForm';
import RentTable from '../../components/rent/RentTable';

// React functional component display rent application form and available cars table to rent 
function Insurance() {
	return (
		<>
			<Row gutter={[16, 16]}>
				<Col span={24}>
					<RentTable />
				</Col>
			</Row>
		</>
	);
}

export default Insurance;
