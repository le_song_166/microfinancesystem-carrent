import React from 'react';
import { Row, Col } from 'antd';
import RentForm from '../../components/rent/RentForm';
import CarsTable from '../../components/carManagement/CarsTable';

// React functional component display rent application form and available cars table to rent 
function Rent() {
	return (
		<>
			<Row gutter={[16, 16]}>
				<Col span={24}>
					<RentForm />
				</Col>
				<Col span={24}>
					<CarsTable />
				</Col>
			</Row>
		</>
	);
}

export default Rent;
