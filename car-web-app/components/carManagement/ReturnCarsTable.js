import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, message, Modal, Form, InputNumber, Input, Space, Button } from 'antd';
import { CloseCircleOutlined } from '@ant-design/icons';
import UserContext from '../../stores/userContext';
import { getApi } from '../../util/fetchApi';
import SmartContractContext from '../../stores/smartContractContext';

// React functional component to display cars details.
// This will return table of cars.
function ReturnCarsTable() {
	const { user } = useContext(UserContext);
	const { confirm , info } = Modal;
	const { RentalContract, UserIdentityContract, MicroTokenContract } = useContext(SmartContractContext);

    const state = [
        'Listed', 
        'OnLoan', 
        'Return', 
        'PayFine', 
        'Completed'
    ];

	const [data, setData] = useState([]);
	const [isModalVisible, setIsModalVisible] = useState(false);
	const [carId, setCarId] = useState('');
	const [carModel, setCarModel] = useState('');
	const [licensePlate, setLicensePlate] = useState('');
	const [pricePerDay, setPricePerDay] = useState('');
	
	//Fetch borrowers details from the User Identity smart contract.
	const getBorrowers = async () => {
	try {
 		const response = await UserIdentityContract.methods.getAllBorrowers().call();

		setData([]);

	 		// Update data array using brokers data returned from User Identity smart contract.
			for (let i = 0; i < response.length; i++) {
	 			const row = {
	 				key: response[i].id,
	 				borrowerid: response[i].id,
					borrowerSocialId: response[i].socialSecurityId,
	 				borrowerAddress: response[i].walletAddress,
 					borrowerName: response[i].name,
					status: response[i].state,
			};

	 			setData((prev) => {
					return [...prev, row];
				});
			}
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading borrowers');
		}
	};

	const getCarCompanies = async () => {
		try {
			 const response = await UserIdentityContract.methods.getAllCarOwners().call();
	
			setData([]);
	
				 // Update data array using brokers data returned from User Identity smart contract.
				for (let i = 0; i < response.length; i++) {
					 const row = {
						 key: response[i].id,
						 carOwnerid: response[i].id,
						 carOwnerSocialId: response[i].socialSecurityId,
						 carOwnerAddress: response[i].walletAddress,
						 carOwnerName: response[i].name,
						 status: response[i].state,
				};
	
					 setData((prev) => {
						return [...prev, row];
					});
				}
			} catch (err) {
				console.log(err);
				message.error('Error occured while loading borrowers');
			}
		};

	const getRentals = async () => {
		try {
			const response = await RentalContract.methods.getRentals().call();
            console.log(response)
            setData([]);
            if(response.length > 0){
                for (let i = 0; i < response.length; i++) {
                    const row = {
                        key: response[i].carId,
                        carId: response[i].carId,
                        rentId: response[i].rentId,
						rentee: response[i].rentee,
						totalDays: response[i].totalDays,
                        due: response[i].due,
                        damageFine: response[i].damageFine,
                        status: response[i].state,
                    };
                    setData((prev) => {
                        return [...prev, row];
                    });
                }
            }
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading car-rental requests');
		}
	};

	const fetchCars = async () => {
		try {
			const response = await getApi({
				url: 'cars',
			});
			const cars = await response;
			setData([]);
			for (let i = 0; i < cars.length; i++) {
				const row = {
					key: cars[i]._id,
					carId: cars[i]._id,
					carModel: cars[i].model,
					licensePlate: cars[i].licensePlate,
					pricePerDay: cars[i].pricePerDay,
				};

				setData((prev) => {
					return [...prev, row];
				});
			}
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading Cars');
		}
	};

	const fetchCarById = async (carId) => {
		try {
			const response = await getApi({
				url: 'cars/' + carId,
			});

			const car = await response;
			setCarId(car._id);
			setCarModel(car.model);
			setLicensePlate(car.licensePlate);
			setPricePerDay(car.pricePerDay);
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading Car');
		}
	};

	const loadData = async () => {
		await getBorrowers();
		await getCarCompanies();
		await getRentals();
		await fetchCarById();
		
	};

	const damageCheck = async (values) => {
		try {
			const accounts = await window.ethereum.enable();

			await RentalContract.methods.checkDamage(
				values.rentId,
				values.damage,
				values.fine,
			).send({ from: accounts[0] });
			message.success('Check Damage successfully');
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error checking damage');
		}
	};

	//const damageCheck = async (rentId) => {
		//try {
			//const accounts = await window.ethereum.enable();
			//await RentalContract.methods.checkDamage(parseInt(rentId)).send({ from: accounts[0] });
			//message.success(`Car ${rentId} Check Damage`);
			//loadData();
		//} catch (err) {
			//console.log(err);
			//message.error('Error occured while approving check damage');
		//}
	//};

	//const confirmDamageCheck = (rentId) => {
		//confirm({
			//content: `Car ${rentId} Check Damage ?`,
			//okText: 'Confirm Check Damage',
			//onOk: () => damageCheck(rentId),
		//});
	//};

	const fineReceived = async (rentId) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.confirmDamagePaymentReceived(parseInt(rentId)).send({ from: accounts[0] });
			message.success(`Car ${rentId} Fine Received`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while approving fine');
		}
	};

	const confirmFineReceived = (rentId) => {
		confirm({
			content: `Approve Car ${rentId} Fine Received?`,
			okText: 'Approve Fine Received',
			onOk: () => fineReceived(rentId),
		});
	};

	const rentalComplete = async (rentId) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.complete(parseInt(rentId)).send({ from: accounts[0] });
			message.success(`Car Rental ${rentId} completed`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while approving car return');
		}
	};

	const confirmRentalComplete = (rentId) => {
		confirm({
			content: `Approve Car Rental ${rentId} Completion ?`,
			okText: 'Approve Car Rental Completed',
			onOk: () => rentalComplete(rentId),
		});
	};

	const showModal = (value) => {
		setIsModalVisible(true);
	};

	const handleCancel = () => {
		setIsModalVisible(false);
		//setIsInsuranceModalVisible(false);
		//setIsInsuranceTransferModalVisible(false);
		//setIsBrokerTransferModalVisible(false);
		//setIsBorrowerTransferModalVisible(false);
    };

	// Define brokers table columns.
	// title - name of the column.
	// dataIndex - property name of the object to be display in the column.
	// key - unique key of the column
	// render - The way data should diplay in the column.
	const columns = [
		{
			title: 'Rent ID',
			dataIndex: 'rentId',
			render: text => text,
		},
		{
			title: 'Borrower Address',
			dataIndex: 'rentee',
		},
		{
			title: 'Status',
			dataIndex: 'status',
			render: tag => {
				let color = 'geekblue';
				if (tag === '3') {
					color = 'red';
				} else if (tag === '0' || tag === '1' || tag === '2'|| tag === '4') {
					color = 'green';
				}
				return (
					<Tag color={color} key={tag}>
						{state[tag]}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'car_company') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			render: (record) => {
				let actionBlock = '';
				if (record.status === '2') {
					actionBlock =
					<Button type="primary" ghost onClick={() => showModal(record.rentId)}> Check Damage </Button>;
							
                } else if (record.status === '3') {
					actionBlock =
					<Button type="primary" ghost onClick={() => confirmFineReceived(record.rentId)}> Fine Received </Button>;
				} else if (record.status === '4') {
					actionBlock =
					<Button type="primary" ghost onClick={() => confirmRentalComplete(record.rentId)}> Completed </Button>;
				} 
				return actionBlock;
			},
		});
	}

	// Display modal to check damage.
	// Parameter - carId = Car Id
	//const checkDamage = (carId) => {
		//fetchPlanById(carId); // First fetch the Car details by its id from the car server.
		//setIsModalVisible(true); // Change car edit modal visibility to true.
	//};

	// Complete Rent.
	// Parameter - carId = Car Id
	//const completePlan = (carId) => {
		//confirm({
			//content: `Confirm complete Rental Car #${carId} ?`,
			//okText: 'Complete',
			//onOk: () => confirmComplete(carId),
		//});
    //};
    
    //const confirmComplete = async (carId) => {
		// try {
		// 	const accounts = await window.ethereum.enable();
		// 	await BankLoanContract.methods.approveLoan(loanId).send({ from: accounts[0] });
		// 	message.success(`Loan ${loanId} approved`);
		// 	loadData();
		// } catch (err) {
		// 	message.error('Error occured while approving the Loan');
		// }
	//};

	////const handleOk = async () => {
		//setIsModalVisible(false);
		//onFinish={damageCheck}
	//};


	useEffect(() => {
		loadData();
	}, []); // useEffect will execute only when component render in to the DOM.

	return (
		<>
			<Card title="Rentals" 
				extra={<Button type="primary" ghost onClick={loadData}>Refresh</Button>}>
				<Table
					pagination="true"
					columns={columns}
					dataSource={data}
					// expandable={{
					// 	expandedRowRender,
					// }}
				/>
			</Card>
			<Modal
				title="Check Damage Information"
				visible={isModalVisible}
				//onOk={handleOk} 
				onCancel={handleCancel}
				footer={[
					<Button key="back" onClick={handleCancel}>
					</Button>,
				]}
				
			>
				<Form
					labelCol={{
						span: 5,
					}}
					wrapperCol={{
						span: 18,
					}}
					layout="horizontal"
					size="default"
					onFinish={damageCheck}
					//onFinish={setIsModalVisible(false)}
				>
				<Form.Item label="Rent Id " name="rentId" rules={[{ required: true, message: 'Please enter Rent Id!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter Rent Id"
					/>
				</Form.Item>
				<Form.Item label="Damage" name="damage" rules={[{ required: true, message: 'Please enter Damage exist or not!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter true or false for Damage Check"
					/>
				</Form.Item>
				<Form.Item label="Fine" name="fine" rules={[{ required: true, message: 'Please enter fine amount!' }]}>
					<InputNumber
						min="0"
						style={{ width: '100%' }}
						placeholder="Enter Fine amount"
					/>
				</Form.Item>
				<Form.Item wrapperCol={{
					lg: { span: 14, offset: 3 },
					xl: { span: 14, offset: 2 },
					xxl: { span: 14, offset: 2 } }}
				>
					<Button type="primary" htmlType="submit">Confirm</Button>
				</Form.Item>
				
			</Form>


			</Modal>
		</>
	);

}

export default ReturnCarsTable;
