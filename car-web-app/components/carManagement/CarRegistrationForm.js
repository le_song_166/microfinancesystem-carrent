import React from 'react';
import PropTypes from 'prop-types';
import { Card, Form, InputNumber, Input, Button, message } from 'antd';
import { postApi } from '../../util/fetchApi';

function CarRegistrationForm({ toggleCar, setToggleCar }) {
	const addNewCar = async (values) => {
		try {
			// console.log(values.model);
			// console.log(values.licensePlate);
			// console.log(values.pricePerDay);
			const body = {
				model: values.model,
				licensePlate: values.licensePlate,
				pricePerDay: values.pricePerDay
			};

			const requestOptions = {
				method: 'POST',
				body: JSON.stringify(body),
			};

			const response = await postApi({
				url: 'cars',
				options: requestOptions,
			});

			const result = await response;
			await console.log(result);

			message.success('New Car is added successfully');
			setToggleCar(!toggleCar);
		} catch (err) {
			message.error('Error while adding the new car');
			console.log(err);
		}
	};

	return (
		<Card title="Add New Car" style={{ margin: '0px' }}>
			<Form
				labelCol={{ lg: 4, xl: 3, xxl: 2 }}
				wrapperCol={{ lg: 14, xl: 12, xxl: 10 }}
				layout="horizontal"
				size="default"
				labelAlign="left"
				onFinish={addNewCar}
			>
				<Form.Item label="Model" name="model" rules={[{ required: true, message: 'Please enter Car Model!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter Car Model"
					/>
				</Form.Item>
				<Form.Item label="License Plate" name="licensePlate" rules={[{ required: true, message: 'Please enter License Plate!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter Car License Plate"
					/>
				</Form.Item>
				<Form.Item label="Price Per Day" name="pricePerDay" rules={[{ required: true, message: 'Please enter Price Per Day!' }]}>
					<InputNumber
						min="0"
						style={{ width: '100%' }}
						placeholder="Enter price per day"
					/>
				</Form.Item>
				<Form.Item wrapperCol={{
					lg: { span: 14, offset: 4 },
					xl: { span: 14, offset: 3 },
					xxl: { span: 14, offset: 2 } }}
				>
					<Button type="primary" htmlType="submit">Add New Car</Button>
				</Form.Item>
			</Form>
		</Card>
	);
}

CarRegistrationForm.propTypes = {
	toggleCar: PropTypes.bool.isRequired,
	setToggleCar: PropTypes.func.isRequired,
};

export default CarRegistrationForm;
