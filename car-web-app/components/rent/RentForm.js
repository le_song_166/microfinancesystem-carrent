import React, { useContext } from 'react';
import { Card, Form, InputNumber, Input, Button, message } from 'antd';
import SmartContractContext from '../../stores/smartContractContext';

function RentForm() {

	const { UserIdentityContract } = useContext(SmartContractContext);

	const { RentalContract } = useContext(SmartContractContext); // Get the Rental Contract instance defined in the 'stores/smartContractContext.js'

	// Create car rent request
	// Values parameter contains the field values submitted through the form.
	const createCarRentalRequest = async (values) => {
		try {
			const accounts = await window.ethereum.enable(); // Get the selected account from the metamask plugin.

			await RentalContract.methods.rentRequest(
				values.borrower,
				values.carId,
				values.totalDays
			).send({ from: accounts[0] }); // Meta mask will return the selected account as an array. This array contains only one account address.
			message.success('Car rent requested successfully');
		} catch (err) {
			console.log(err);
			message.error('Error creating car rental request');
		}
	};

	return (
		<Card title="Car Rental Form">
			<Form
				labelCol={{
					lg: 4,
					xl: 3,
					xxl: 2,
				}}
				wrapperCol={{
					lg: 16,
					xl: 14,
					xxl: 10,
				}}
				layout="horizontal"
				size="default"
				labelAlign="left"
				onFinish={createCarRentalRequest} // createLoanRequest function will execute when user submit the loan form.
			>
				{/* Name property value(carId) will use to capture the Input field value when submit the form */}
				<Form.Item label="Car ID" name="carId" rules={[{ required: true, message: 'Please enter car id!' }]}>
					<Input
						placeholder="Enter car id"
					/>
				</Form.Item>
				<Form.Item label="Total Days" name="totalDays" rules={[{ required: true, message: 'Please enter Total Days!' }]}>
					<InputNumber
						min="0"
						style={{ width: '100%' }}
						placeholder="Enter Total Days."
					/>
				</Form.Item>
				<Form.Item label="Wallet Address" name="borrower" rules={[{ required: true, message: 'Please enter wallet address!' }]}>
					<Input
						style={{ width: '100%' }}
						placeholder="Enter borrower's wallet address"
						// addonAfter={<AimOutlined onClick={(e) => setWalletAddress(e)} />}
					/>
				</Form.Item>
				<Form.Item wrapperCol={{
					lg: { span: 14, offset: 4 },
					xl: { span: 14, offset: 3 },
					xxl: { span: 14, offset: 2 } }}
				>
					{/* Form submit button */}
					<Button type="primary" htmlType="submit">Rent</Button>
				</Form.Item>
			</Form>
		</Card>
	);
}

export default RentForm;
