Smart Contract Migration
========================

Smart Contract Dependency Diagram
----------------------------------

The following diagram shows the dependency between smart contracts.
``MicroToken`` smart contract implements the ERC20 Token Interface(``IERC20``). 
``UserIdentity`` smart contract stores the stake holders' details.
``BankLoan``, ``InsurancePolicy``, ``LoanPayment`` smart contracts uses ``UserIdentity`` smart contract to verify the users.

.. image:: ../images/smart_contract_dependency.png

Migration
----------

User Identity and Other Smart Contract Migration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We use following code to deploy UserIdentity and other smart contracts. ::

    const UserIdentity = artifacts.require("UserIdentity");
    const BankLoan = artifacts.require("BankLoan");
    const InsurancePolicy = artifacts.require("InsurancePolicy");

    module.exports = async function(deployer, network, accounts) 
    {
        await deployer.deploy(UserIdentity);
        const userIdentityInstance = await UserIdentity.deployed();

        await deployer.deploy(BankLoan, userIdentityInstance.address);
        await deployer.deploy(InsurancePolicy, userIdentityInstance.address, {from: accounts[1]});
    };

Filename: ``migrations/3_user_identity_migration.js``

As describe in above migration, first we insert all the contracts we'd like to interact with
(UserIdentity, BankLoan, InsurancePolicy).
As shown in the above *Smart Contract Dependency Diagram* ``BankLoan`` and ``InsurancePolicy`` smart contracts 
need ``UserIdentity`` smart contract address to deploy. 
To achieve this first we deploy the *UserIdentity* smart contract. 
We use async/await methods to deploy these contracts.
After deploying the ``UserIdentity`` smart contract we wait for it finishes the deployment.
Then we deploy other 2 smart contract with UserIdentity smart contract address as a parameter.

Bank is the owner of ``UserIdentity`` and ``BankLoan`` smart contracts.
But for the ``InsurancePolicy`` smart contract Insurance Company is the owner.
To fullfill this requirement we deploy ``InsurancePolicy`` smart contract with a different account.
We can define optional parameters as the third argument for the ``deployer.deploy`` function.
We specify the second account of the accounts array as the dployer account for the ``InsurancePolicy`` smart contract.
If not specify the deployer account it will take the first account as the deployer account.


