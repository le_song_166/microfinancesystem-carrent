Quickstart Instructions
=======================

1. Install Dependencies
-----------------------

Install following dependencies in your computer.

1. Install git - To check out the project from the GitLab
2. Install Node - Node will use to install other dependencies like Truffle and Node is the package manager for the React App. This project requires the Node version 14.x.
3. Install Ganache - We use Ganache to run a local blockchain network on your computer.
4. Install MetaMask - MetaMask is used for wallet account management.
5. Install MongoDB - Bank web server uses the MongoDB to stores the data.

2. Checkout the Project from GitLab
-----------------------------------

Open a terminal in your computer and execute the following commands to checkout ``microfinance`` to your computer and 
change git branch to ``level2`` branch. ::

    git clone https://gitlab.com/gmc123/microfinance.git
    cd microfinance/
    git branch level2


3. Deploy Smart Contract to Ganache
------------------------------------

First install all node dependencies for ``blockchain`` peoject using following command. ::

    npm install 

First run Ganache on your computer.

Change directory to ``microfinance/blockchain/`` directory on your terminal and execute following command 
to deploy smart contracts into the Ganache local blockchain network. ::

    truffle migrate --reset

4. Setup MetaMask
-----------------

Connect MetaMask to the Ganache local blockchain.

5. Run Bank Web server
-----------------------

In your terminal change the directory to the ``bank-server`` directory and execute the following commands to 
install ``bank-server`` dependencies and run the ``bank-server``. ::

    npm install
    npm run start

To check the Bank Server running correctly navigate to ``localhost:9091`` in the browser.
If the browser displays the *Welcome to Bank Server* then you can start the Bank Web Application.

6. Install React App Dependencies
---------------------------------

In your terminal change the directory to the ``bank-web-app`` directory.
Then execute the following command to install ``bank-web-app`` dependencies. ::

    npm install

7. Run Bank Web Application
---------------------------

After successfully installing React App dependencies you can execute the following command inside the ``bank-web-app`` directory to run the React App. ::

    npm run dev

Now navigate to ``localhost:3005`` to in the browser to view the Bank Web Application.

8. Connect Bank Web Application to MetaMask
-------------------------------------------

In your **Google Chrome** browser navigate to ``localhost:3005`` and you will be able to see the Bank web app running on your browser.
When you load the Bank web App on your browser MetaMask will ask to connect to Bank wen app (localhost:3005).

9. Run Insurance Company Web Server 
------------------------------------

Open new terminal in ``insurance-server`` directory and run following command one after the other to 
install node dependencies and start the web server. ::

    npm install
    npm start

You can check the Insurance Server by navigating to ``localhost:9092`` in your browser.

10. Run Insurance Web Application
----------------------------------

After successfully start the Insurance Web Server you can open new terminal on ``insurance-web-app`` directory
and execute following commands. ::

  npm install
  npm run dev

Now you can navigate to ``localhost:3010`` on your browser to view the Insurance Web Appliaction.

Load Data
---------

You can open a new terminal on ``example-data`` directory and first run the following command. ::

    npm install

This will install all the node modules for the ``example-data`` project.
Then run the following command to register users in the ``UserIdentity`` smart contract. ::

    npm start

This will create the users as follows. 

.. list-table::
   :header-rows: 1

   * - User Role
     - User Id
     - Name
     - Wallet Address
     - SSN.
     - Status
   * - Broker
     - 1
     - Leonard Hofstadter
     - *Ganache Account 2*
     - 2345
     - APPROVED
   * - Borrower
     - 1
     - Rajesh Koothrapali
     - *Ganache Account 3*
     - 3455
     - APPROVED
   * - Insurance Company
     - 1
     - ABC Insurance Company
     - *Ganache Account 4*
     - 9876
     - APPROVED

*Ganache Account 1* address is used by Bank.