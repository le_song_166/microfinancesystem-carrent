Layered Architecture
====================

**Insurance Company Web Server** follows a layered architecture.
It consists of theree layers.

* API Layer - API of the Insurance Company Web Server.
* Service Layer - Implemets the server side business logic.
* Data Layer - Implements database.

Following diagram shows the layered architecture of the **Insurance Company Web Server**.

.. image:: ../images/insurance_server_architecture.png 

API Layer
---------

**Insurance Company Web Server** servers two rotes; ``/policy-plans`` and ``/policy-payments``.
As mentioned in the Index.js section, requests comming to these routes will redirect to
the ``routes/plans`` and ``routes/payments`` scripts.

Both the routes will work in same manner. 
We will take Plan route as an example to describe following sections.

In ``routes/plans`` script first we import required dependencies as follows. ::

    const Express = require('express') // Define Express web framework
    const router = Express.Router() // Import router module from Express.
    const planService = require('../services/planService'); // Import plan service script.

``plans`` router serves 4 HTTP request methods (GET, POST, PATCH, and DELETE).

**Get All Policy Plans**

The following function return all policy plans from the MongoDB.
When user sends a GET request to the ``/policy-plans`` url in **Insurance Company Web Server** 
it will call ``planService.getPlans()`` function and returns policy plans to the user. ::

    router.get('/', async (req, res) => {
        try {
            const plans = await planService.getPlans();
            res.json(plans);
        }
        catch (err) {
            res.json({
                message: err
            })
        }
    })

These requests contain no parameters in the url and no json objects in the request body.

**Get Policy Plan By Policy Plan Id**

The following function return a policy plan for the given plan id.
This function will pass the GET request to the ``planService.getPlanById()`` function and returns policy plan. ::

    router.get('/:planId', async (req, res) => {
        try{
            const plan = await planService.getPlanById(req);
            res.json(plan);
        }
        catch(err){
            res.json({
                message: err
            })
        }
    })

These requests contain Policy Plan Id in the url. No json objects in the request body.

**Save Policy Plan**

When user sends a POST request to the ``/policy-plans`` it will pass the request to the ``planService.createPlan()`` 
function and returns the saved policy plan. ::

    router.post('/', async (req, res) => {
        try {
            const savedPlan = await planService.createPlan(req);
            res.json(savedPlan);
        }
        catch (err) {
            res.json({
                message: err
            })
        }
    })

These requests contain no parameters in the url but json object in the request body.

**Update Policy Plan**

When user sends PATCH request to the ``/policy-plans`` it will update the existing Policy Plan in the MongoDB 
specified by the Policy Plan Id. It will call the ``planService.updatePlan()`` function with the request and return the 
updated Policy Plan. ::

    router.patch('/:planId', async (req, res) => {
        console.log(req.params.planId);
        try {
            const updatedPlan = await planService.updatePlan(req)
            res.json(updatedPlan);
        }
        catch (err) {
            res.json({
                message: err
            })
        }
    })

These requests contain Policy Plan Id in the url and Policy Plan json object in the request body.

**Delete Policy Plan**

When user sends a DELETE request to the ``/policy-plans`` url it will delete the Policy Plan specified by the Policy Plan Id
in the request url. It will call the ``planService.deletePlan()`` function with the request 
and returns the deleted Policy Plan. ::

    router.delete('/:planId', async (req, res) => {
        console.log(req.params.planId);
        try {
            const plan = await planService.deletePlan(req);
            if(plan.deletedCount==0){
                res.status(404).send('Policy Plan not found');
            }
            res.json(plan);
        }
        catch (err) {
            res.json({
                message: err
            })
        }
    })

These requests contains the Policy Plan Id in request url and no json objects in the request body.

After run your **Insurance Company Web Server** you can navigate to ``localhost:9092/api-docs`` to see the
full Swagger API documentation for all APIs served by **Insurance Company Web Server**.

Service Layer
-------------

We use service layer to implement business logic in the server.
The requests coming to the route will sends to the respective service layer methods.
As we mentioned before we will discuss about Policy Plans Service Layer implementation in this section.

First we import the model defined in the ``models/Plans`` script.
It defines the Mongoose Schema to interact with MongoDB. ::

    const Plan = require('../models/Plans')

Then we define the ``planService`` as follows. ::
    
    const planService = {
        getPlans: async (req) => {
            const plans = await Plan.find()
            return plans;
        },
        getPlanById: async (req) => {
            const plans = await Plan.findById(req.params.planId);
            return plans;
        },
        createPlan: async (req) => {
            const plan = new Plan({
                months: req.body.months,
                loanAmount: req.body.loanAmount,
                initialPayment: req.body.initialPayment,
                finalPayment: req.body.finalPayment
            })
            const savedPlan = await plan.save();
            return savedPlan;
        },
        updatePlan: async (req) => {
            const updatedPlan = await Plan.updateOne({ _id: req.params.planId },
                {
                    $set: {
                        months: req.body.months,
                        loanAmount: req.body.loanAmount,
                        initialPayment: req.body.initialPayment,
                        finalPayment: req.body.finalPayment
                    }
                });
            return updatedPlan;
        },
        deletePlan: async (req) => {
            const deletedPlan = await Plan.deleteOne({ _id: req.params.planId });
            return deletedPlan;
        },
    }

``planService`` functions will use Mongoose Schema Queries to interact with the MongoDB.
You can learn more about these queries in `Mongoose Queries page <https://mongoosejs.com/docs/queries.html>`_

**getPlans()**

This method will find and return all Policy Plans from the MongoDB using Plan schema. 
It will use ``find`` Mongoose Query to get all policy plans from the MongoDB.

**getPlanById()**

This function will get the Policy Plan Id from the request url, find and return the Policy Plan using Plan schema.
It will use ``findById`` Mongoose Query to get the Policy Plan by Id from the MongoDB.

**createPlan()**

In this function it will create new Policy Plan in the MongoDB.
First it will create a Plan object using json object received from the request body.
Then it will use ``save`` Mongoos Query to save the new Policy Plan in the MongoDB.

**updatePlan()**

In this function it will update the existing Policy Plan specified by the Policy Plan Id.
It will get the Policy Plan Id from the request url and updated fields from the request body.
It will use the ``updateOne`` Mongoose Query to update the object in the MongoDB.

**deletePlan()**

This function will delete the Policy Plan in the MongoDB using ``deleteOne`` Mongoose Query.
The Policy Plan Id will send as a request url parameter.

Data Layer
-----------

Data Layer represent the MongoDB.
This node server uses Mongoose to interact with the MongoDB.
It defines the schemas in the ``models`` directory.

We defined the Plans Schema as follows. ::

    const mongoose = require('mongoose')

    const PlanSchema = mongoose.Schema({
        months: {
            type: Number,
            required: true
        },
        loanAmount: {
            type: Number,
            required: true
        },
        payment: {
            type: Number,
            required: true
        },
    })

    module.exports = mongoose.model('Plans', PlanSchema)

We use the auto generated ``_id`` field for the Policy Plans.
Other than that Policy Plan has 5 fields. 

* ``months`` - Duration of the Bank Loan.
* ``loanAmount`` - token amount of the Bank Loan.
* ``payment`` - Insurance Company fee for the Policy.

Each field was defined with it's type and required status.
These Schemas were used in the ``planService`` to query the MongoDB.