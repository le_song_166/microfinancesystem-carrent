.. _project-structure-target:

Project Structure
=================

Here is the folder structure of Microfinance system. This system consists of 5 main projects 
and one project for documentation as shown below. ::

    Microfinance
    |--bank-server
    |--bank-web-app
    |--blockchain
    |--docs
    |--insurance-server
    |--insurance-web-app


1. ``bank-server/``: Node Server project for Bank.
2. ``bank-web-app/``: React web application for Bank.
3. ``blockchain/``: Truffle project for develop and deploy smart contracts
4. ``docs/``: Directory for Sphinx documnets.
5. ``insurance-server/``: Node Server application for Insurance Company.
6. ``insurance-web-app/``: React web application for Insurance Company.

Deployment Diagram
-------------------

.. image:: ../images/deployment.png

As shown in the diagram the Bank and the Insurance Company have web applications developed using React.
These Bank and Insurance Company web applications connect to the Bank Server and the Insurance Server respectively.
These React web applciations to Node Servers connection use HTTP.
These servers consist of Node.js HTTP server component and a Mongo DB database component. 
Both the Bank and the Insurance Company web applications connect to blockchain using Web3.
