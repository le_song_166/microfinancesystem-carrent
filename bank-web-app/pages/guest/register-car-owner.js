import React from 'react';
import { Row, Col } from 'antd';
import CarOwnerRegistrationForm from '../../components/userManagement/CarOwnerRegistrationForm';

function RegisterCarOwner() {
	return (
		<>
			<Row gutter={[16, 16]}>
				<Col span={24}>
					<CarOwnerRegistrationForm />
				</Col>
			</Row>
		</>
	);
}

export default RegisterCarOwner;
